CREATE TABLE IF NOT EXISTS Usuario( 
    consecutivo INTEGER PRIMARY KEY, 
    nombres VARCHAR(100) NOT NULL, 
    apellidos VARCHAR(100) NOT NULL,
    tipo_documento VARCHAR(100) NOT NULL,
    documento INTEGER NOT NULL,
    fecha_nacimiento Date,
    telefono VARCHAR(11) NULL,
    correo VARCHAR(30) NULL
);

