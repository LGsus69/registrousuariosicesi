import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { DatabaseService } from '../database/database.service';
import { SQLitePorter } from '@ionic-native/sqlite-porter/ngx';
import { HttpClient } from '@angular/common/http';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends DatabaseService  {


  constructor(protected plt: Platform,
              protected sqlitePorter: SQLitePorter,
              protected sqlite: SQLite,
              protected http: HttpClient,
              public DB: DatabaseService) { 
    super( plt, sqlitePorter, sqlite, http );
  }

 
  guardarUsuario( nombres, apellidos, tipo_documento, documento, fecha_nacimiento, telefono, correo ): Promise<any> {
    return this.isReady()
    .then(() => {
      return new Promise((resolve, reject) => {
        this.db.executeSql(
          'INSERT INTO Usuario ( nombres, apellidos, tipo_documento, documento, fecha_nacimiento, telefono, correo ) \
          VALUES ( ?, ?, ?, ?, ?, ?, ? )',
          [
            nombres,
            apellidos,
            tipo_documento,
            documento,
            fecha_nacimiento,
            telefono,
            correo
          ]
        )
          .then((result) => {
            resolve(true);
          })
          .catch(e => {
            alert('Error al guardar usuario: ' + JSON.stringify(e));
            reject(false);
          });
      });
    });
  }

async eliminarUsuario(documento): Promise<any> {
    return this.isReady()
      .then(() => {
        return new Promise((resolve, reject) => {
          this.db.executeSql(
            'DELETE FROM Usuario WHERE documento = ?',
            [documento]
          )
            .then((result) => {
              resolve(true);
            })
            .catch(e => {
              alert('Error al borrar usuario: ' + JSON.stringify(e));
              reject(false);
            });
        });
      });
  }

  async updateUsuario( nombres, apellidos, tipo_documento, documento, fecha_nacimiento, telefono, correo, consecutivo ): Promise<any> {
    return this.isReady()
      .then(() => {
        return new Promise((resolve, reject) => {
          this.db.executeSql('UPDATE Usuario SET nombres = ?, apellidos = ?, tipo_documento = ?, documento = ?, fecha_nacimiento = ?,\
                             telefono = ?, correo = ? WHERE consecutivo = ?',
            [
              nombres,
              apellidos,
              tipo_documento,
              documento,
              fecha_nacimiento,
              telefono,
              correo,
              consecutivo
            ])
            .then((data) => {
              resolve(true);
            })
            .catch(error => alert('updateUsuario: ' + JSON.stringify(error)));
        });
      });

  }
  
}
