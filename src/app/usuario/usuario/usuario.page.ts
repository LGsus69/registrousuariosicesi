import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/database/usuario.service';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { DatabaseService } from 'src/app/services/database/database.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {

  consecutivo: any;
  nombres: any;
  apellidos: any;
  tipo_documento: any;
  documento: any;
  fecha_nacimiento: any;
  telefono: any;
  correo: any;
  
  editable: any;

  tipos_documento: any[];
  estados: any[];
  estado: any;

  item: any = {};
 

  constructor( public usuarioDb: UsuarioService,
                public toastController: ToastController,
                private router: Router,
                private route: ActivatedRoute,
                private services: DatabaseService,
                private activatedRoute: ActivatedRoute,
                public alertController: AlertController,
                public navCtrl: NavController) { 

                  this.route.queryParams.subscribe(params => {
                    this.documento = params['documento'];
                    console.log('params: ', this.documento);
                });

                activatedRoute.params.subscribe(data => {
                  this.editable = data.editable;
                });
                }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    this.tipos_documento = this.services.tipos_documento();

    this.getUsuarios()
      .then((lista) => {
        if(lista){  
          console.log('Lista usuario editar: ', lista);        
          this.item             = lista;
          this.consecutivo      = this.item.consecutivo;
          this.nombres          = this.item.nombres;
          this.apellidos        = this.item.apellidos;
          this.documento        = this.item.documento;
          this.tipo_documento   = this.item.tipo_documento;
          this.telefono         = this.item.telefono;       
          this.correo           = this.item.correo;       
         } else { console.log('NO EXISTE LA LISTA');}
        
      }).catch(error => console.log(error));
  }

  async getUsuarios() {
    return this.services.getInfoUsuarios( this.documento );
  }

  registrarUsuario() {
    console.log('info insert: ', this.nombres, this.apellidos, this.tipo_documento, this.documento, this.fecha_nacimiento, this.telefono, this.correo);
    this.usuarioDb.guardarUsuario(this.nombres, this.apellidos, this.tipo_documento, this.documento, this.fecha_nacimiento, this.telefono, this.correo)
      .then(() => {
        let mensaje = 'Se ha registrado al usuario ' + this.nombres
        this.toastCreate(mensaje);
        //this.router.navigate(['home']);
        this.navCtrl.navigateRoot(['home']);
      });
  }

  editarUsuario(item) {
    this.usuarioDb.updateUsuario(this.nombres, this.apellidos, this.tipo_documento, this.documento, this.fecha_nacimiento, this.telefono, this.correo, item.consecutivo)
      .then(() => {
        let mensaje = 'Se ha actualizado al usuario ' + this.nombres
        this.toastCreate(mensaje);
        this.navCtrl.navigateRoot(['home']);
      });
  }

  async toastCreate(mensaje) {
    //this.db.updateEstado(item.id_orden, 1, 0);
    const toast = await this.toastController.create({
      message: mensaje,
      duration: 2000
    });
    toast.present();
  }

  salir(){
    let mensaje = 'Se ha cancelado el registro'
    this.toastCreate(mensaje);
    this.navCtrl.navigateRoot(['home']);
  }


  printText(event) {
    console.log( this.tipo_documento, this.nombres, this.estado)
  }

}
